this is my version of a Flac to Opus script

The opus codec is used in a ogg file because it's easier to use for some Android phones.

Usage:

./flac2opus.sh /path/to/flac/files /path/to/opus/files


all metadata including cover etc will be copied to the ogg file